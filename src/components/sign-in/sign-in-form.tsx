import { SignInSchema, signInSchema } from "@/utils/schemas";
import { Button, FormControlLabel, TextField } from "@mui/material";
import { SubmitHandler, useForm } from "react-hook-form";
import { zodResolver } from "@hookform/resolvers/zod";
import { IOSSwitch } from "../ui/ios-style-switch";
import Link from "next/link";
import { GoogleSigninButton } from "../ui/google-signin-button";

export function SignInForm() {
  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm<SignInSchema>({ resolver: zodResolver(signInSchema) });

  const onSubmit: SubmitHandler<SignInSchema> = (data) => console.log(data);
  return (
    <div className=" max-w-[480px] flex flex-col gap-20">
      <div className="border border-white  rounded-3xl p-8">
        <form
          onSubmit={handleSubmit(onSubmit)}
          className="flex flex-col gap-6 "
        >
          <div className="flex gap-5">
            <img src="/assets/login/user.svg" alt="" />
            <TextField
              error={!!errors.email}
              helperText={errors.email?.message}
              variant="filled"
              label="Email"
              type="text"
              disabled
              className="w-full"
              {...register("email", { required: true })}
            />
          </div>

          <div className="flex gap-5 ">
            <img src="/assets/login/lock.svg" alt="" />
            <TextField
              className="w-full"
              error={!!errors.password}
              helperText={errors.password?.message}
              label="Password"
              variant="filled"
              type="password"
              disabled
              {...register("password", { required: true })}
            />
          </div>

          <div className="text-white flex lg:items-center justify-between ml-[60px] lg:flex-row flex-col gap-2 lg:gap-0">
            <FormControlLabel
              control={<IOSSwitch sx={{ m: 1 }} defaultChecked />}
              label="Lembrar-me"
            />
            <Link href="/">Esqueci minha senha</Link>
          </div>
          <Button type="submit" className="bg-deepBlue font-medium">
            Entrar
          </Button>
          <GoogleSigninButton />
        </form>
      </div>

      <div className="flex gap-6">
        <img src="/assets/login/logo-booster.svg" alt="" />
        <p className="text-white">
          Digitando... <br /> Olá! Eu sou o Booster. Que bom que você está aqui.
          Juntos, podemos transformar seus dados em insights que irão
          impulsionar seus negócios!
        </p>
      </div>
    </div>
  );
}
