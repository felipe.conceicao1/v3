"use client"

import { Key } from "react"
import PushPinIcon from '@mui/icons-material/PushPin';
import { Tags } from "@/components/ui/tags";

export const ChatWithBotSummaryCard = (props: any) => {
    const { chatTitle, chatTime, lastMessage, tags, isActive, isPinned, id } = props
    return (
        <button onClick={() => { }} className=" w-full" id={id}>
            <div className={`${isActive ? 'bg-[#002935]' : 'bg-darkTeal'} pt-4 pl-4 pr-2 border-2 border-x-0 border-[#13131386] w-full`}>
                <div className="flex pr-1">
                    <div className="flex flex-col flex-1 gap-5">
                        <div className="flex justify-between items-center">
                            <div className="flex flex-col">
                                <h2 className="text-white font-normal text-sm mb-[2px]">
                                    {chatTitle}
                                </h2>

                            </div>
                            <div className="flex flex-col items-end gap-2 text-sm">
                                <time className="text-lightGray">{chatTime}</time>
                            </div>
                        </div>
                        <div className="flex gap-2 text-sm text-[#8F9293]">
                            {lastMessage}
                        </div>
                    </div>
                </div>
                <div className="flex justify-between mt-3.5 pb-2 items-end">
                    {tags.map((tagName: string, key: Key) => <Tags key={key} >{tagName}</Tags>)}
                    {isPinned ? <PushPinIcon color="info" /> : null}
                </div>
            </div>
        </button>
    )
}