import { SignInScreen } from "./page.client";

export default function SignIn() {
  return (
    <div className="bg-[url('/assets/login/login-bg.svg')] ">
      <SignInScreen />
    </div>
  );
}
