"use client";
import { ChatWithBotTopNav } from "@/components/dashboard/chat-with-bot/chat-with-bot-topnav";
import { ChatWithBotInput } from "@/components/dashboard/chat-with-bot/chat-with-bot-input";
import { ChatWithBotUserMessageBubble } from "@/components/dashboard/chat-with-bot/chat-with-bot-user-message-bubble";
import { ChatWithBotBotMessageBubble } from "@/components/dashboard/chat-with-bot/chat-with-bot-bot-message-bubble";
import { useEffect, useRef, useState } from "react";

export default function ChatWithBot() {
  const [textBotUser, setTextBotUser] = useState<any>([]);

  const ref = useRef<HTMLDivElement>(null);
  useEffect(() => {
    if (textBotUser.length) {
      ref.current?.scrollIntoView({
        behavior: "smooth",
        block: "end",
      });
    }
  }, [textBotUser.length]);

  return (
    <div className="flex flex-1 flex-col w-full m-4 ">
      <div className="flex flex-1 overflow-y-hidden h-full">
        <div className="w-full bg-[#002F3C] h-full">
          <ChatWithBotTopNav chatTitle={"Nova conversa"} />
          <div className="px-10 mt-5 flex flex-col gap-2.5 overflow-y-auto h-[80%]">
            {textBotUser.map((messageData: any, i: number) => {
              if (messageData.robot) {
                return (
                  <ChatWithBotBotMessageBubble
                    key={i}
                    messageData={messageData.robot}
                  />
                );
              } else {
                return (
                  <ChatWithBotUserMessageBubble
                    key={i}
                    messageData={messageData.user}
                  />
                );
              }
            })}
            <div ref={ref} />
          </div>
        </div>
      </div>
      <ChatWithBotInput setTextBotUser={setTextBotUser} />
    </div>
  );
}