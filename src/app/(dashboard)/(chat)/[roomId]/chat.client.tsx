import { ChatTopNav } from "@/components/dashboard/chat/chat-top-nav";
import { ChatBox } from "@/components/dashboard/chat/chat-box";

export default function ChatScreen() {
  return (
      <div className="flex flex-1 flex-col w-full m-4">
        <ChatTopNav />
        <ChatBox />
      </div>
  );
}
